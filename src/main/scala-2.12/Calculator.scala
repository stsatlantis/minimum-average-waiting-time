import scala.collection.mutable

object Calculator {

  private implicit def customerOrdering: Ordering[Customer] =
    (c1: Customer, c2: Customer) => {
      c2.pizzaCookingTime compare c1.pizzaCookingTime
    }

  def calculateMinimumWaitingTime(customers: mutable.ListBuffer[Customer]): Long = {
    val customerCount = customers.size
    val customerQueue = new mutable.PriorityQueue[Customer]()
    var sumWaitingTime = 0L
    var currentTime = 0L

    while (customers.nonEmpty || customerQueue.nonEmpty) {
      while (customers.nonEmpty && (customerQueue.isEmpty || customers.head.arriveTime <= currentTime)) {
        val customer = customers.head
        customers -= customer
        customerQueue.enqueue(customer)
        currentTime = Math.max(currentTime, customer.arriveTime)
      }

      val customer = customerQueue.dequeue()
      currentTime += customer.pizzaCookingTime
      sumWaitingTime += currentTime - customer.arriveTime
    }

    sumWaitingTime / customerCount
  }

}
