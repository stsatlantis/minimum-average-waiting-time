import org.scalatest.{Matchers, WordSpecLike}

import scala.collection.mutable

object Helper {

  def readFile(path: String): mutable.ListBuffer[Customer] = {
    val customerBuffer = mutable.ListBuffer.empty[Customer]
    for (line <- io.Source.fromResource(path).getLines.drop(1)) {
      val lineValues = line.split(" ").map(_.toLong)
      customerBuffer += Customer(lineValues.head, lineValues(1))
    }
    customerBuffer.sortWith(_.arriveTime < _.arriveTime)
  }

}

class CalculatorTest extends WordSpecLike with Matchers {

  "Calculator " should {

    "return 9" in {
      val customers = Helper.readFile("sample1.txt")
      Calculator.calculateMinimumWaitingTime(customers) shouldBe 9
    }

    "return 8" in {
      val customers = Helper.readFile("sample2.txt")
      Calculator.calculateMinimumWaitingTime(customers) shouldBe 8
    }

    "return 6" in {
      val customers = Helper.readFile("sample3.txt")
      Calculator.calculateMinimumWaitingTime(customers) shouldBe 6
    }

    "return 6667863382" in {
      val customers = Helper.readFile("sample4.txt")
      Calculator.calculateMinimumWaitingTime(customers) shouldBe 6667863382L
    }

    "return 8323528195457" in {
      val customers = Helper.readFile("sample5.txt")
      Calculator.calculateMinimumWaitingTime(customers) shouldBe 8323528195457L
    }

    "return 49999" in {
      val customers = Helper.readFile("sample6.txt")
      Calculator.calculateMinimumWaitingTime(customers) shouldBe 49999
    }

  }
}
